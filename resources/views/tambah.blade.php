@extends('layouts.main')
@section('container')
    <div class="inputan mt-5">
        <h3>Tambah Data</h3>
        <form action="{{ route ('jdw.create') }}" method="POST">
            @csrf
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Nama Tim</label>
                <input type="text" class="form-control" id="tim" placeholder="Nama Tim" name="tim">
            </div>
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Tanggal Main</label>
                <input type="text" class="form-control" id="tgl" placeholder="Tanggal Main" name="tgl_spar">
            </div>
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Alamat</label>
                <input type="text" class="form-control" id="alamat" placeholder="Alamat" name="alamat">
            </div>
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Tempat</label>
                <input type="text" class="form-control" id="tempat" placeholder="Tempat Main" name="tempat">
            </div>
            <button type="submit" class="btn btn-primary btn-sm">Simpan</button>
        </form>
    </div>
@endsection
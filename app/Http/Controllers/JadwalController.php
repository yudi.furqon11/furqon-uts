<?php

namespace App\Http\Controllers;

use App\Models\jadwal;
use Illuminate\Http\Request;

class JadwalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = jadwal::all(); 

        return view('jadwal', compact('data'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new jadwal();

        $save = $data->create([
            'tim' => $request->tim,
            'tgl_spar' => $request->tgl_spar,
            'alamat' => $request->alamat,
            'tempat' => $request->tempat
        ]);

        if ($save) {
            $data = jadwal::all();
            return view('jadwal', compact('data'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\jadwal  $jadwal
     * @return \Illuminate\Http\Response
     */
    public function show(jadwal $jadwal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\jadwal  $jadwal
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = jadwal::find($id);

        return view('update', compact('data'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\jadwal  $jadwal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = jadwal::find($id);

        $update = $data->update([
            'tim' => $request->tim,
            'tgl_spar' => $request->tgl_spar,
            'alamat' => $request->alamat,
            'tempat' => $request->tempat,
        ]);

        if ($update) {
            $data = jadwal::all();
            return view('jadwal', compact('data'));
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\jadwal  $jadwal
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = jadwal::find($id);
        $data->delete();

        return redirect()->route('jdw.index')->with('toast_error', 'Data berhasil di hapus');

    }
}

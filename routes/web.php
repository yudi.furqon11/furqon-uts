<?php

use App\Http\Controllers\JadwalController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('home');
});
Route::get('/about', function () {
    return view('about');
});

Route::prefix('jadwal')->group(function() {
    Route::get('/', [JadwalController::class, 'index'])->name('jdw.index');
    Route::get('/tambah',[JadwalController::class, 'create'])->name('jdw.create');
    Route::post('/tambah',[JadwalController::class, 'store'])->name('jdw.create');
    Route::get('/update/{id}',[JadwalController::class, 'edit'])->name('jdw.edit');
    Route::put('/update/{id}',[JadwalController::class, 'update'])->name('jdw.edit');
    Route::delete('delete/{id}', [JadwalController::class, 'destroy'])->name('jdw.delete');
});


